import random


def lottery_game():
    rnd = random.sample(range(1, 51), 10)
    rnd.sort()
    return rnd


if __name__ == "__main__":
    random_list = lottery_game()
    print(random_list)

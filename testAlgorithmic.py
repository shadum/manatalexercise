import unittest
from algorithmic import roman_numeral


class TestRandomness(unittest.TestCase):

    def test_equal(self):
        self.assertEqual(roman_numeral(1), 'I')
        self.assertEqual(roman_numeral(2), 'II')
        self.assertEqual(roman_numeral(3), 'III')
        self.assertEqual(roman_numeral(4), 'IV')
        self.assertEqual(roman_numeral(5), 'V')
        self.assertEqual(roman_numeral(6), 'VI')
        self.assertEqual(roman_numeral(7), 'VII')
        self.assertEqual(roman_numeral(8), 'VIII')
        self.assertEqual(roman_numeral(9), 'IX')
        self.assertEqual(roman_numeral(10), 'X')
        self.assertEqual(roman_numeral(11), 'XI')
        self.assertEqual(roman_numeral(20), 'XX')
        self.assertEqual(roman_numeral(30), 'XXX')
        self.assertEqual(roman_numeral(40), 'XL')
        self.assertEqual(roman_numeral(50), 'L')
        self.assertEqual(roman_numeral(65), 'LXV')
        self.assertEqual(roman_numeral(87), 'LXXXVII')
        self.assertEqual(roman_numeral(92), 'XCII')
        self.assertEqual(roman_numeral(101), 'CI')
        self.assertEqual(roman_numeral(244), 'CCXLIV')
        self.assertEqual(roman_numeral(525), 'DXXV')
        self.assertEqual(roman_numeral(728), 'DCCXXVIII')
        self.assertEqual(roman_numeral(990), 'CMXC')
        self.assertEqual(roman_numeral(1001), 'MI')
        self.assertEqual(roman_numeral(3999), 'MMMCMXCIX')


if __name__ == '__main__':
    unittest.main()

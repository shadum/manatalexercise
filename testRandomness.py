import unittest
from randomness import lottery_game


class TestRandomness(unittest.TestCase):

    def test_length(self):
        self.assertEqual(len(lottery_game()), 10)

    def test_unique(self):
        self.assertEqual(len(set(lottery_game())), 10)

    def test_in_scope(self):
        lottery_prize = lottery_game()
        min_num = min(lottery_prize)
        max_num = max(lottery_prize)
        self.assertGreaterEqual(min_num, 1)
        self.assertLessEqual(min_num, 50)
        self.assertGreaterEqual(max_num, 1)
        self.assertLessEqual(max_num, 50)


if __name__ == '__main__':
    unittest.main()

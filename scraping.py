from bs4 import BeautifulSoup
import requests

# Twitter now block web scraping. We have to use Twitter's api to get follower data.
# This code just show the concept of web scraping.


def get_follower_num(twitter_url, user):
    url = twitter_url
    res = requests.get(url)
    res.encoding = "utf-8"
    if res.status_code == 200:
        soup = BeautifulSoup(res.text, features="html.parser")
        for di in soup.find_all('div'):
            for a in di.find_all('a', href=f'{user}/followers'):
                for follower in a.find_all('span'):
                    print(follower.string)


if __name__ == '__main__':
    url = "https://twitter.com/KMbappe"
    user = url.split('/')[-1]
    get_follower_num(url, user)

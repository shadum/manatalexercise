def roman_numeral(num):
    if not isinstance(num, int):
        return 'input number should be integer'
    if num > 3999:
        return 'input number cannot greater than 3999'
    if num < 1:
        return 'input number cannot less than 1'

    digit_list = list()
    digit_list.append(assemble(num // 1000, ["M", "-", "-"]))
    digit_list.append(assemble(num % 1000 // 100, ["C", "D", "M"]))
    digit_list.append(assemble(num % 100 // 10, ["X", "L", "C"]))
    digit_list.append(assemble(num % 10, ["I", "V", "X"]))

    character = ''
    for char in digit_list:
        if char:
            character = f'{character}{char}'

    return character


def assemble(num, list_of_char):
    if len(list_of_char) != 3:
        return False
    switch = {
        1: list_of_char[0],
        2: f'{list_of_char[0]}{list_of_char[0]}',
        3: f'{list_of_char[0]}{list_of_char[0]}{list_of_char[0]}',
        4: f'{list_of_char[0]}{list_of_char[1]}',
        5: f'{list_of_char[1]}',
        6: f'{list_of_char[1]}{list_of_char[0]}',
        7: f'{list_of_char[1]}{list_of_char[0]}{list_of_char[0]}',
        8: f'{list_of_char[1]}{list_of_char[0]}{list_of_char[0]}{list_of_char[0]}',
        9: f'{list_of_char[0]}{list_of_char[2]}',
    }
    return switch.get(num, False)


if __name__ == "__main__":
    pass
